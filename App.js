/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React, { Component, Fragment } from 'react';
import OneSignal from 'react-native-onesignal';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Platform
} from 'react-native';

import AppNavigator from './src/navigation';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';



/*********************************************** StatusBarPlaceHolder *********************************************/
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

function StatusBarPlaceHolder() {
  return (
    <View style={{
      width: "100%",
      height: STATUS_BAR_HEIGHT,
      backgroundColor: "#339b8b"
    }}>
      <StatusBar barStyle="light-content" />
    </View>
  );
}

class App extends Component {
  constructor(props, properties) {
    super(props, properties);
    OneSignal.setLogLevel(6, 0);
    OneSignal.setSubscription(true);
    OneSignal.init("4c335227-fbdb-4286-a0b6-b56870ce15f0");
    OneSignal.registerForPushNotifications();
    this.state = {
      accessToken: '',
      clientId: '',
      uidId: '',
      profileData: [],
      notificationTotalRead: [],
      onesignalAppId: ''
    };
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds.bind(this));
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
    this.setState({
      onesignalAppId: device.userId
    }, () => this.postOneSignalUserId())
  }


  componentDidMount() {
    OneSignal.getPermissionSubscriptionState((status) => {
      console.log("OneSignal----------status", status);
      console.log("OneSignal----------status===", status.subscriptionEnabled);
      
    });

    OneSignal.checkPermissions((permissions) => {
      // console.log("permissions=========", permissions);
    });

    /************************************  AsyncStorage ACCESS_TOKEN ***************************************/
    AsyncStorage.getItem("ACCESS_TOKEN").then(accessTokenAsynRes => {
      this.setState({
        accessToken: accessTokenAsynRes
      })
    })
    /************************************  AsyncStorage CLIENT ***************************************/
    AsyncStorage.getItem("CLIENT").then(clientIdAsynRes => {
      this.setState({
        clientId: clientIdAsynRes
      })
    })
    /************************************  AsyncStorage UID ***************************************/
    AsyncStorage.getItem("UID").then(uidIdAsynRes => {
      this.setState({
        uidId: uidIdAsynRes
      })
    })
    /************************************  AsyncStorage PROFILE_DATA ***************************************/
    AsyncStorage.getItem("PROFILE_DATA").then(profileAsynRes => {
      this.setState({
        profileData: JSON.parse(profileAsynRes)
      })
    })
    this.getNotification();
    this.postOneSignalUserId();
  }

  /*********************************************** getNotification *********************************************/
  getNotification() {
    const { accessToken, clientId, uidId } = this.state;
    let baseIp = "https://erp-ice.com/api/v1/notifications";
    let axiosConfig = {
      headers: {
        'access-token': accessToken,
        'client': clientId,
        'uid': uidId
      }
    };
    console.log('dsss', accessToken, clientId, uidId)
    let that = this;
    axios.get(baseIp, axiosConfig)
      .then((fetchData) => {
        console.log('fetchData----leaveType', fetchData);
        if (fetchData.status === 200) {
          that.setState({
            notificationTotalRead: fetchData.data.unread_count
          })
        }
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  /*********************************************** postOneSignalUserId *********************************************/
  postOneSignalUserId() {
    let baseIp = "https://erp-ice.com/api/v1//users/update_user";
    let outputJson = [
      {
        user: {
          one_signal_id : this.state.onesignalAppId
        }
      }
    ]

    let that = this;
    console.log('outputJson---postOneSignalUserId------------------', outputJson)
    axios.post(baseIp, {
      user: {
        one_signal_id : this.state.onesignalAppId
      }
    })
      .then((res) => {
        // console.log('login----',JSON.stringify(res));
        if (res.status === 200) {
          console.log('res: -------',res)
        }
      })
      .catch(function (err) {
        console.log("postOneSignalUserId-----", err);
      });
  }


  render() {
    const { accessToken, clientId, uidId, profileData, notificationTotalRead, onesignalAppId } = this.state;
    console.log('onesignalAppId==================================', onesignalAppId)
    // const dataPastProfile = profileData.data || []
    return (
      <Fragment>
        {
          Platform.OS === 'ios' ? <StatusBarPlaceHolder /> :
            <StatusBar barStyle="light-content" backgroundColor="#339b8b" />
        }
        <AppNavigator
          screenProps={{
            dataNotiTotalRead: notificationTotalRead
            // access_token: accessToken,
            // client: clientId,
            // uid: uidId,
          }}
        />
      </Fragment>
    );
  }
}

export default App;




const styles = StyleSheet.create({

});

