import React, { Component } from 'react';
import {
  StyleSheet, Dimensions, View, TouchableOpacity, ImageBackground, ScrollView, Image, Modal,
  ActivityIndicator
} from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import axios from 'axios';
import RNRestart from 'react-native-restart';
import AsyncStorage from '@react-native-community/async-storage';

/*********************************************** React Navigation *********************************************/
import { createAppContainer, createSwitchNavigator, SafeAreaView } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerNavigatorItems } from 'react-navigation-drawer';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

/*********************************************** RN Vector Icon *********************************************/
import ANT from 'react-native-vector-icons/AntDesign';
import FA from 'react-native-vector-icons/FontAwesome';
import ETY from 'react-native-vector-icons/Entypo';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

/*********************************************** Import Page Components *********************************************/
import LoginScreen from '../components/login';
import HomeScreen from '../components/home';
import AttendanceScreen from '../components/attendance';
import LeaveScreen from '../components/leave';
import CameraClockInScreen from '../components/attendance/cameraClockIn';
import AttendanceRecordScreen from '../components/attendance/attendanceRecord';

/* Leave module screen */
import NewLeaveApplicationScreen from '../components/leave/application/new';
import ShowLeaveApplicationScreen from '../components/leave/application/show';
import LeaveApplicationSubmitted from '../components/leave/application/submitted';
import LeaveApplicationPending from '../components/leave/application/pending';
import LeaveApplicationApproved from '../components/leave/application/approved';
import LeaveApplicationCancelled from '../components/leave/application/cancelled';
import LeaveApplicationRejected from '../components/leave/application/rejected';
import ShowLeaveApprovalScreen from '../components/leave/approval/show';
import LeaveApprovalApproved from '../components/leave/approval/approved';
import LeaveApprovalPending from '../components/leave/approval/pending';
import LeaveApprovalRejected from '../components/leave/approval/rejected';

import ClaimScreen from '../components/claim';
import FinancialClaimScreen from '../components/claim/financial';
import OTScreen from '../components/claim/ot';
import AnnouncementScreen from '../components/announcement';
import NotificationScreen from '../components/notification';
import ProfileScreen from '../components/profile';
import ChangePasswordScreen from '../components/profile/changePassword';
import NotificationListScreen from '../components/notification/notificationList';


const image = "https://images.pexels.com/photos/1054289/pexels-photo-1054289.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";


/*********************************************** StyleSheet *********************************************/
const styles = StyleSheet.create({
  menustyWarp: { justifyContent: 'center', flex: 1, paddingLeft: 10, paddingRight: 50, height: '100%', },
  menusty: { color: '#fff', },
  menustyClose: { color: '#fff', paddingRight: 20 },
  closeWarp: { height: 70, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center', backgroundColor: '#44c1ac' },
  iconRignt: { color: "#fff", paddingRight: 10 }
})

/*********************************************** DrawerButton *********************************************/
const DrawerButton = (props) => {
  return (
    <View>
      <TouchableOpacity onPress={props.navigation.toggleDrawer} style={styles.menustyWarp}>
        <ANT name="menu-unfold" size={20} style={styles.menusty} />
      </TouchableOpacity>
    </View>
  );
};

/*********************************************** RightButton *********************************************/
const RightButton = (props, screenProps) => {
  console.log("screenProps", props.screenProps)
  return (
    <View style={{ flexDirection: 'row' }} >
      {/* <TouchableOpacity style={{ paddingLeft: 3 }} onPress={() => props.navigation.navigate('announcement')}>
        <ANT name="infocirlce" size={23} style={[styles.iconRignt, { paddingLeft: 10 }]} />
      </TouchableOpacity> */}
      <TouchableOpacity style={{ paddingLeft: 3 }} onPress={() => props.navigation.navigate('notification')}>
        <MCI name="bell" size={23} style={styles.iconRignt} />
        {
          props.screenProps.dataNotiTotalRead.length > 0 ?
            (<View style={{
              padding: 3, backgroundColor: 'blue', borderRadius: 100,
              alignItems: 'center', position: 'absolute', right: 5, 
            }}>
            <Text note style={{ fontSize: 8, color: '#fff' }}>12</Text>
            </View>)
            : 
            (null)
        }
      </TouchableOpacity>
    </View>
  );
};

/*********************************************** CameraClockInButton *********************************************/
const CameraClockInButton = (props, screenProps) => {
  return (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity style={{ paddingLeft: 3 }} onPress={() => props.navigation.navigate('attendance_record')}>
        <FA name="file-text" size={22} style={[styles.iconRignt, { paddingLeft: 10 }]} />
      </TouchableOpacity>
    </View>
  );
};


/*********************************************** createMaterialTopTabNavigator *********************************************/
const MyLeaveApplication = createMaterialTopTabNavigator({
  submittedTab: {
    screen: LeaveApplicationSubmitted,
    navigationOptions: ({ navigation }) => ({
      title: 'Submitted',
    })
  },
  approvedTab: {
    screen: LeaveApplicationApproved,
    navigationOptions: {
      title: 'Approved',
    }
  },
  rejectedTab: {
    screen: LeaveApplicationRejected,
    navigationOptions: {
      title: 'Rejected',
    }
  },
  cancelledTab: {
    screen: LeaveApplicationCancelled,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Cancelled',
    })
  },
  /* pendingTab: {
    screen: LeaveApplicationPending,
    navigationOptions: {
      title: 'Pending',
    }
  }, */
}, {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        tabBarOptions: {
          style: {
            backgroundColor: 'white',
            elevation: 0, // remove shadow on Android
            shadowOpacity: 0, // remove shadow on iOS,
            borderWidth: 1,
            borderColor: '#ccc',
          },
          activeTintColor: '#f48120',
          inactiveTintColor: '#ccc',
          labelStyle: {
            fontWeight: 'bold',
            fontSize: 12
          },
          indicatorStyle: {
            backgroundColor: '#f48120',
          },
          // scrollEnabled: true,
        },
      };
    }
  });

const MyLeaveApproval = createMaterialTopTabNavigator({
  pendingTab: {
    screen: LeaveApprovalPending,
    navigationOptions: {
      title: 'Pending',
    }
  },
  approvedTab: {
    screen: LeaveApprovalApproved,
    navigationOptions: {
      title: 'Approved',
    }
  },
  rejectedTab: {
    screen: LeaveApprovalRejected,
    navigationOptions: {
      title: 'Rejected',
    }
  }
}, {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        tabBarOptions: {
          style: {
            backgroundColor: 'white',
            elevation: 0, // remove shadow on Android
            shadowOpacity: 0, // remove shadow on iOS,
            borderWidth: 1,
            borderColor: '#ccc',
          },
          activeTintColor: '#f48120',
          inactiveTintColor: '#ccc',
          labelStyle: {
            fontWeight: 'bold',
            fontSize: 12
          },
          indicatorStyle: {
            backgroundColor: '#f48120',
          },
          // scrollEnabled: true,
        },
      };
    }
});

// const StackHome = createStackNavigator(
//   {
//    Home: Home,
//    CustomHide: CustomHide,
//   });
//   // This code let you hide the bottom app bar when "CustomHide" is rendering
//   StackHome.navigationOptions = ({ navigation }) => {
//    let tabBarVisible;
//     if (navigation.state.routes.length > 1) {
//   navigation.state.routes.map(route => {
//     if (route.routeName === "CustomHide") {
//       tabBarVisible = false;
//     } else {
//       tabBarVisible = true;
//     }
//   });
//    }

//    return {
//      tabBarVisible
//    };
//   };


/*********************************************** createStackNavigator *********************************************/

///////////////////////////// HomeStackScreen /////////////////////////////////// 
const HomeStackScreen = createStackNavigator({
  // login: {
  //   screen: LoginScreen,
  //   navigationOptions: {
  //     header: null,
  //     headerBackTitle: null,
  //   }
  // },
  home: {
    screen: HomeScreen,
    navigationOptions: ({ navigation, screenProps, props }) => ({
      title: 'Home',
      headerLeft: <DrawerButton navigation={navigation} />,
      headerRight: <RightButton navigation={navigation} screenProps={screenProps} />,
    })
  },
  notification: {
    screen: NotificationScreen,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Notification',
    })
  },
  notification_list : {
    screen: NotificationListScreen,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Notification List',
    })
  },

  attendance: {
    screen: AttendanceScreen,
    navigationOptions: ({ navigation, screenProps }) => ({
      title: 'Attendance',
      // headerRight: <CameraClockInButton navigation={navigation} screenProps={screenProps} />,
    })
  },
  leave: {
    screen: LeaveScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Leave',
    })
  },
  new_leave_application: {
    screen: NewLeaveApplicationScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Leave Application',
    })
  },
  show_leave_application: {
    screen: ShowLeaveApplicationScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'My Submission',
    })
  },
  show_leave_approval: {
    screen: ShowLeaveApprovalScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'My Approval',
    })
  },

  my_submission: {
    screen: MyLeaveApplication,
    navigationOptions: ({ navigation }) => ({
      title: 'My Submission',
    })
  },
  cc_to_me: {
    screen: MyLeaveApplication,
    navigationOptions: ({ navigation }) => ({
      title: 'CC to ME',
    })
  },
  my_approval: {
    screen: MyLeaveApproval,
    navigationOptions: ({ navigation }) => ({
      title: 'My Approval',
    })
  },

  cameraClockIn: {
    screen: CameraClockInScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Camera Clock In',
    })
  },

  attendance_record: {
    screen: AttendanceRecordScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Attendance Record',
    })
  },
  claim: {
    screen: ClaimScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Financial Claim',
    })
  },
  financial: {
    screen: FinancialClaimScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Financial Claim',
    })
  },
  ot: {
    screen: OTScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Over Time(OT)',
    })
  },
  announcement: {
    screen: AnnouncementScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Announcement',
    })
  },




}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#44c1ac', },
      headerTintColor: '#fff',
      // headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })


///////////////////////////// ProfileStackScreen /////////////////////////////////// 
const ProfileStackScreen = createStackNavigator({
  profile: {
    screen: ProfileScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Profile',
      headerLeft: <DrawerButton navigation={navigation} />,
    })
  },
  change_pass: {
    screen: ChangePasswordScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Change Password',
    })
  },
}, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: { backgroundColor: '#44c1ac', },
      headerTintColor: '#fff',
      // headerTitleStyle: { fontWeight: 'bold' },
      headerBackTitle: '  ',
      headerBack: '  '
    }
  })


/*********************************************** CustomDrawerComponent *********************************************/
const CustomDrawerComponent = (props, screenProps) => {
  function logoutFuc() {
    // //console.log('logoutFuc-navi')
    // AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID", "PROFILE_DATA"]).then(response => {
    //   let baseIp = "https://erp-ice.com/api/v1/auth/sign_out";
    //   let axiosConfig = {
    //       headers: {
    //       'access-token': response[0][1],
    //       'client': response[1][1],
    //       'uid': response[2][1],
    //     }
    //   }
    //   axios.delete(baseIp, axiosConfig).then(response => {
    //     if (response.data.success) {
    //       AsyncStorage.removeItem('ACCESS_TOKEN');
    //       AsyncStorage.removeItem('CLIENT');
    //       AsyncStorage.removeItem('UID');
    //       RNRestart.Restart();
    //     }
    //   });
    // });
    AsyncStorage.removeItem('ACCESS_TOKEN');
    AsyncStorage.removeItem('CLIENT');
    AsyncStorage.removeItem('UID');
    RNRestart.Restart();
  }
  // const dataProfile = props.screenProps.profileList || []

  // console.log('dataProfile=====================',dataProfile)
  return (
    <SafeAreaView forceInset={{ top: 'never', bottom: 'never' }}>
      <View style={styles.closeWarp}>
        <TouchableOpacity onPress={logoutFuc} style={{ paddingLeft: 20 }}>
          <ANT name="logout" size={30} style={styles.menustyClose} />
        </TouchableOpacity>
        <TouchableOpacity onPress={props.navigation.closeDrawer} style={{}}>
          <ANT name="close" size={30} style={styles.menustyClose} />
        </TouchableOpacity>
      </View>
      <CardItem style={{ backgroundColor: '#44c1ac', paddingBottom: 20 }}>
        <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Thumbnail source={{ uri: image }} style={{ width: 80, height: 80, borderRadius: 100, marginBottom: 10 }} />
          {/* <Text style={{ color: '#fff', fontWeight: 'bold', marginBottom: 5 }}>{dataProfile.full_name}</Text>
          <Text style={{ color: '#fff', fontWeight: 'bold' }}>{dataProfile.company_name}</Text> */}
        </Body>
      </CardItem>
      <ScrollView>
        <DrawerNavigatorItems {...props} />
      </ScrollView>
    </SafeAreaView>
  )
}

/*********************************************** createDrawerNavigator *********************************************/
const DrawerNavigator = createDrawerNavigator({
  home: {
    name: 'Home',
    screen: HomeStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'Home',
      drawerIcon: ({ tintColor }) => (
        <ANT name="home" size={25} style={{ color: "#44c1ac" }} />
      )
    }),
  },
  profile: {
    name: 'Profile',
    screen: ProfileStackScreen,
    navigationOptions: () => ({
      drawerLabel: 'Profile',
      drawerIcon: ({ tintColor }) => (
        <ANT name="solution1" size={25} style={{ color: "#44c1ac" }} />
      )
    })
  },
}, {
    initialRouteName: 'home',
    contentComponent: props => <CustomDrawerComponent {...props} />,
    drawerType: 'front',
    drawerBackgroundColor: '#fff',
    //lazy: 'false',
    drawerWidth: 328,
    drawerPosition: 'left',
    contentOptions: {
      labelStyle: { color: '#000', fontSize: 20, fontWeight: 'normal', },
      activeLabelStyle: { color: '#44c1ac', },
      itemsContainerStyle: { marginTop: 0, },
      // itemStyle: { borderBottomWidth: .5, borderBottomColor: '#ffffff2e', }
    }
  })


/*********************************************** createSwitchNavigator *********************************************/
const AppNavigator = createSwitchNavigator({
  NavigatStack: { screen: DrawerNavigator }
});


export default createAppContainer(AppNavigator)

