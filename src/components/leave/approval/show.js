import React, { Component } from 'react';
import { Alert, View, StyleSheet, ScrollView, TextInput } from 'react-native';
import { Text, Button, Card, CardItem, Body } from 'native-base';
import axios from 'axios';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import { ConfirmDialog } from 'react-native-simple-dialogs';

class ShowLeaveApprovalScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      record: [],
      comment: "",
      commentModal: false,
      commentType: ""
    };
  }

  componentDidMount() {
    this.getRecord();
  }

  onChangeTextInput = (value) => {
    this.setState({
      comment: value
    })
  };

  /*********************************************** getRecord *********************************************/
  getRecord = () => {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/approvals/" + this.props.navigation.state.params.id;
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1]
        }
      }

      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((res) => {
          if (res.status === 200) {
            that.setState({
              record: res.data
            })
          }
        })
        .catch(function (error) {
          console.log(error);
        })
    });
  };

  onConfirmComment = (type) => {
    console.log("CHEKKKK")
    this.setState({
      commentModal: true,
      commentType: type
    })
  };

  submitApprove = () => {
    this.setState({
      commentModal: false
    })

    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/approvals/" + this.props.navigation.state.params.id + "/approve";
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1]
        }
      }

      let that = this;
      axios.post(baseIp, { approval: { comment: this.state.comment } }, axiosConfig)
        .then((res) => {
          if (res.data.success) {
            Alert.alert('Notice', res.data.success,
              [
                {
                  text: 'OK', onPress: () => { 
                    this.props.navigation.navigate('show_leave_approval', { 
                      id: this.props.navigation.state.params.id 
                    }), 
                    this.getRecord(); 
                  }
                },
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert('Error', res.data.error,
              [
                {
                  text: 'OK', onPress: () => this.props.navigation.navigate('home')
                },
              ],
              { cancelable: false }
            );
          }
        })
        .catch(function (error) {
          console.log(error);
        })
    });
  };

  submitReject = () => {
    this.setState({
      commentModal: false
    })

    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/approvals/" + this.props.navigation.state.params.id + "/reject";
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1]
        }
      }

      let that = this;
      axios.post(baseIp, { approval: { comment: this.state.comment } }, axiosConfig)
        .then((res) => {
          if (res.data.success) {
            Alert.alert('Notice', res.data.success,
              [
                {
                  text: 'OK', onPress: () => { 
                    this.props.navigation.navigate('show_leave_approval', { 
                      id: this.props.navigation.state.params.id 
                    }), this.getRecord(); 
                  }
                },
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert('Error', res.data.error,
              [
                {
                  text: 'OK', onPress: () => this.props.navigation.navigate('home')
                },
              ],
              { cancelable: false }
            );
          }
        })
        .catch(function (error) {
          console.log(error);
        })
    });
  };

  render() {
    console.log("this.state.record =====", this.state.record)

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ padding: 10 }}>
            <Card>
              <CardItem>
                <Body>
                  <Text>{this.state.record.description}</Text>
                  <Text>{this.state.record.duration}</Text>
                  <Text>{this.state.record.category}</Text>
                  <Text>{this.state.record.submitted_by}</Text>
                  <Text>{this.state.record.submitted_at}</Text>
                  <Text>{this.state.record.status}</Text>
                </Body>
              </CardItem>
            </Card>
          </View>
        </ScrollView>
        {
          this.state.record.approvable ?
          (
            <View>
              <Button full success style={{ height: 60 }} onPress={() => this.onConfirmComment('approve')}>
                <Text>APPROVE</Text>
              </Button>
              <Button full danger style={{ height: 60 }} onPress={() => this.onConfirmComment('reject')}>
                <Text>REJECT</Text>
              </Button>
            </View>
          ) : 
          (
            null
          )
        }

        <ConfirmDialog
            title="Comment"
            visible={this.state.commentModal}
            onTouchOutside={() => this.setState({commentModal: false})}
            positiveButton={{
                title: "Submit",
                onPress: () => { 
                  if (this.state.commentType == 'approve') {
                    this.submitApprove();  
                  } else {
                    this.submitReject();
                  }
                  
                }
            }}
            negativeButton={{
                title: "Cancel",
                onPress: () => this.setState({commentModal: false})           
            }} >
            <View>
              <Card style={{ padding: 10, marginBottom: 5 }}>
                <TextInput
                  value={this.state.comment}
                  placeholder="Enter comment here..."
                  placeholderTextColor="#8c8c8c"
                  onChangeText={(value) => this.onChangeTextInput(value)}
                  autoCorrect={false}
                  autoCapitalize="none"
                />
              </Card>
            </View>
        </ConfirmDialog>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
})

export default ShowLeaveApprovalScreen;