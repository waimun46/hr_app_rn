import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { Text, Card, CardItem, Body } from 'native-base';
import axios from 'axios';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

class LeaveApprovalRejected extends Component {
  constructor(props) {
    super(props);
    this.state = {
      approvals: []
    };
  }

  componentDidMount() {
    this.getRecords();
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      this.getRecords();
    });
  }

  /*********************************************** getRecords *********************************************/
  getRecords() {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/approvals";
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1]
        }
      }

      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((response) => {
          console.log('response----leaveType', response);
          if (response.status === 200) {
            that.setState({
              approvals: response.data.approvals
            })
            console.log("checkapprovals", response)
          }
        })
        .catch(function (error) {
          console.log(error);
        })  
    })
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          {
            this.state.approvals && this.state.approvals.length > 0 ?
              this.state.approvals.map((item, i) => {
                return (
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('show_leave_approval', { id: item.id }) }}>
                    <Card>
                      <CardItem>
                        <Body>
                          <Text>{item.description}</Text>
                          <Text>{item.duration}</Text>
                          <Text>{item.category}</Text>
                          <Text>{item.submitted_by}</Text>
                          <Text>{item.submitted_at}</Text>
                          <Text>{item.status}</Text>
                        </Body>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                )
              })
            :
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', paddingVertical: '50%'}}>
              <Text>No Record</Text>
            </View>
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { padding: 10 },
})

export default LeaveApprovalRejected;