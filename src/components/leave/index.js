import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, FlatList, ScrollView } from 'react-native';
import { Card, Text, Thumbnail, List, ListItem, Left, Body, Right, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';

class LeaveScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const navigateAction = this.props.navigation.navigate;
    const propsData = this.props.navigation.state.params;
    return (
      <View style={styles.container}>
        <ScrollView>
          {/******************************** LEAVE PROCESS ******************************* */}
          <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
            <TouchableOpacity style={{ alignItems: 'center', marginRight: 15 }} onPress={() => navigateAction('my_submission', { title: 'my_submission' })}>
              <View style={styles.borderSty}>
                <ANT name="form" size={40} style={{ color: '#44c1ac' }} />
              </View>
              <Text note style={{ color: '#000' }}>My Submission</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => navigateAction('cc_to_me', { title: 'cc_to_me' })}>
              <View style={styles.borderSty}>
                <ANT name="select1" size={40} style={{ color: '#44c1ac' }} />
              </View>
              <Text note style={{ color: '#000' }}>CC to Me</Text>
            </TouchableOpacity> */}
            <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => navigateAction('my_approval', { title: 'my_approval' })}>
              <View style={styles.borderSty}>
                <ANT name="carryout" size={40} style={{ color: '#44c1ac' }} />
              </View>
              <Text note style={{ color: '#000' }}>My Approval</Text>
            </TouchableOpacity>
          </View>

          {/******************************** CREATE NEW ******************************* */}
          <View style={{ backgroundColor: '#eee', marginTop: 30, padding: 15 }}>
            <Text style={{ color: '#000', fontWeight: 'bold' }}>Create New</Text>
          </View>


          {/******************************** LEAVE APPLICATION ******************************* */}

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30 }}>
            <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => navigateAction('new_leave_application',
              {
                accessToken: propsData.access_token,
                clientId: propsData.client,
                uidId: propsData.uid,
              }
            )}>
              <View style={styles.borderSty}>
                <ANT name="profile" size={50} style={{ color: '#44c1ac' }} />
              </View>
              <Text note style={{ color: '#000' }}>Leave Application</Text>
            </TouchableOpacity>
          </View>


        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', padding: 30, },
  borderSty: {
    borderWidth: 2, padding: 10, borderRadius: 50, width: 80, height: 80, borderColor: '#eee',
    alignItems: 'center', justifyContent: 'center', marginBottom: 10,
  },
  iconContainer: { alignItems: 'center', justifyContent: 'center', marginBottom: 10, },

})

export default LeaveScreen;
