import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { Text, Card, CardItem, Body } from 'native-base';
import axios from 'axios';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

class LeaveApplicationSubmitted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leaves: []
    };
  }

  componentDidMount() {
    this.getRecords();
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      this.getRecords();
    });
  }

  /*********************************************** getRecords *********************************************/
  getRecords() {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/leave_applications/submitted";
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1],
        }
      }

      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((fetchData) => {
          console.log('fetchData----leaveType', fetchData);
          if (fetchData.status === 200) {
            that.setState({
              leaves: fetchData.data.leave_applications
            })
          }
        })
        .catch(function (error) {
          console.log(error);
        })  
    })
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          {
            this.state.leaves && this.state.leaves.length > 0 ?
              this.state.leaves.map((item, i) => {
                return (
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('show_leave_application', { id: item.id }) }}>
                    <Card>
                      <CardItem>
                        <Body>
                          <Text>{item.leave_category}</Text>
                          <Text>{item.description}</Text>
                          <Text>{item.date_start} - {item.date_end}</Text>
                          <Text>{item.duration}</Text>
                          <Text>{item.status}</Text>
                        </Body>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                )
              })
            :
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', paddingVertical: '50%'}}>
              <Text>No Record</Text>
            </View>
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { padding: 10 },
})

export default LeaveApplicationSubmitted;