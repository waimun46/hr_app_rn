import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, TextInput, ScrollView, Alert, CheckBox } from 'react-native';
import { Card, Text, Title, Button, Header, Left, Body, Right, Picker, Icon, Thumbnail, DatePicker, Item, Input } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';
import { ConfirmDialog } from 'react-native-simple-dialogs';
import AsyncStorage from '@react-native-community/async-storage';

class NewLeaveApplicationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: '',
      remark: '',
      agree_unpaid: false,
      selectedLeaveType: "",
      selectedStartype: "",
      selectedEndType: "",
      startDate: moment().format('YYYY-MM-DD'),
      endDate: moment().format('YYYY-MM-DD'),
      duration: '',
      ImageSource: null,
      Image_TAG: '',
      ImgData: null,
      validationMessageModal: false,
      isSubmit: false,
      onConfirmSubmitModal: false,
      onConfirmSubmitModalData: [],
      leaveType: [],
    };
    this.onValueChange = this.onValueChange.bind(this)
  }

  componentDidMount() {
    this.getLeaveType();
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'description') { this.setState({ description: text }) }
    if (field === 'remark') { this.setState({ remark: text }) }
  }

  /****************************************** onValueChange ********************************************/
  onValueChange(value: string, field) {
    if (field === 'selectedLeaveType') { this.setState({ selectedLeaveType: value }) }
    if (field === 'selectedStartype') { this.setState({ selectedStartype: value }) }
    if (field === 'selectedEndType') { this.setState({ selectedEndType: value }) }
  }

  /*********************************************** getLeaveType *********************************************/
  getLeaveType() {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/leave_applications/leave_categories_list";
      let axiosConfig = {
        headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1]
        }
      };
      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((fetchData) => {
          console.log('fetchData----leaveType', fetchData);
          if (fetchData.status === 200) {
            that.setState({
              leaveType: fetchData.data.leave_categories
            })
          }
        })
        .catch(function (error) {
          console.log(error);
        })
    });
  }

  /*********************************************** takePhoto *********************************************/
  takePhoto() {
    // console.log('showImagePicker-----')
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          ImageSource: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          ImgData: response.data
        });
      }
    });
  }

  formValidation = () => {
    if (this.state.startDate) {
      this.setState({
        validationMessageModal: true
      })
    }
  };

  submitLeave() {
    // console.log('submit leave----');
    // console.log('clockIn------');

    const navigateAction = this.props.navigation.navigate;
    const { startDate, endDate, remark, selectedStartype, selectedEndType,
      selectedLeaveType, ImgData, description, agree_unpaid } = this.state;

      this.setState({
        isSubmit: true
      })

      AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
        let baseIp = "https://erp-ice.com/api/v1/leave_applications";
        let postData = {
          leave_application: {
            date_start: moment(startDate).format('DD/MM/YYYY'),
            date_end: moment(endDate).format('DD/MM/YYYY'),
            time_start: selectedStartype,
            time_end: selectedEndType,
            description: description,
            remarks: remark,
            leave_category_id: selectedLeaveType,
            file: ImgData ,
            agree_unpaid: agree_unpaid == true ? agree_unpaid : ''
          }
        };
        let axiosConfig = {
          headers: {
            'Content-Type': 'application/json',
            'access-token': response[0][1],
            'client': response[1][1],
            'uid': response[2][1]
          }
        };
        let that = this;
        axios.post(baseIp, postData, axiosConfig)
          .then((res) => {
            if (res.status === 200) {
              if (res.data.success) {
                Alert.alert('Notice', res.data.success,
                  [
                    {
                      text: 'OK', onPress: () => navigateAction('my_submission',
                        that.setState({
                          date_start: '',
                          date_end: '',
                          time_start: '',
                          time_end: '',
                          description: '',
                          remarks: '',
                          leave_category_id: '',
                          file: null,
                          agree_unpaid: '',
                          onConfirmSubmitModal: false,
                        }))
                    },
                  ],
                  { cancelable: false }
                );
              } else {
                Alert.alert('Error', res.data.error);
              }
            }
          })
          .catch((err) => {
            alert("Submit Failed, Please try again");
          })
          // console.log('outputJson----', postData)
        });
  }

  onConfirmSubmit() {
    const { description, startDate, endDate, selectedStartype, selectedEndType,
      selectedLeaveType } = this.state;

    if (description && selectedLeaveType && startDate && endDate && selectedStartype && selectedEndType) {

      AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
        let calculateBalanceBaseIp = "https://erp-ice.com/api/v1/leave_applications/calculate_balance";
        let calculateBalancePostData = {
          leave_application: {
            date_start: moment(startDate).format('DD/MM/YYYY'),
            date_end: moment(endDate).format('DD/MM/YYYY'),
            time_start: selectedStartype,
            time_end: selectedEndType,
            leave_category_id: selectedLeaveType,
          }
        };
        
        let axiosConfig = {
          headers: {
            'Content-Type': 'application/json',
            'access-token': response[0][1],
            'client': response[1][1],
            'uid': response[2][1]
          }
        };

        let that = this;
        axios.post(calculateBalanceBaseIp, calculateBalancePostData, axiosConfig)
          .then((res) => {
            if (res.status === 200) {
              if (res.data.need_agree_unpaid == true) {
                this.setState({
                  onConfirmSubmitModal: true,
                  onConfirmSubmitModalData: res.data
                })
              } else {
                this.submitLeave();
              }

            } else {
              //
            }
          })
          .catch((err) => {
            console.log("Response 200 Error")
          })
      });
    } else {
      this.setState({
        validationMessageModal: true
      })
    }
  }


  render() {
    const { description, endDate, remark, startDate, selectedLeaveType, leaveType, selectedStartype,
      selectedEndType, ImageSource } = this.state;
    const navigateAction = this.props.navigation.navigate;

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ padding: 10, paddingBottom: 30 }}>
            {/********************* description FOR LEAVE *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>1. Description</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <TextInput
                value={description}
                placeholder="Description..."
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'description')}
                autoCorrect={false}
                autoCapitalize="none"
              // keyboardType={'numeric'}
              />
            </Card>
            {/********************* LEAVE TYPE *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>2. Leave Type</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <Picker
                required
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                headerBackButtonText="back"
                placeholder="Select Leave Type"
                selectedValue={selectedLeaveType}
                // onValueChange={this.onValueChange.bind(this)}
                onValueChange={(value) => this.onValueChange(value, 'selectedLeaveType')}
                renderHeader={backAction =>
                  <Header style={{ backgroundColor: "#44c1ac" }}>
                    <Left>
                      <Button transparent onPress={backAction}>
                        <Icon name="arrow-back" style={{ color: "#fff" }} />
                      </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                      <Title style={{ color: "#fff" }}>Select One</Title>
                    </Body>
                    <Right />
                  </Header>}
              >
                <Picker.Item label="Select Leave Type" value={null} />
                {
                  leaveType !== '' && leaveType.map((item, i) => {
                    {
                      if (item.balance !== '') { 
                        return (<Picker.Item label={item.name + " (" + item.balance + ")"} value={item.id} />)
                      } else {
                        return (<Picker.Item label={item.name} value={item.id} />)
                      }
                    }
                  })
                }
              </Picker>
            </Card>
            {/********************* START DATE *******************/}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              {/********************* DAY *******************/}
              <Card style={{ padding: 10, marginBottom: 5, width: '49%', marginRight: 0, marginLeft: 0, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ marginRight: 5 }}>4. Start Date</Text>
                  <Text style={{ color: 'red' }}>*</Text>
                </View>
                <DatePicker
                  required
                  disabled={selectedLeaveType !== "" ? false : true}
                  style={{ alignItems: 'flex-start', width: '100%', }}
                  date={startDate}
                  mode="date"
                  hideText={false}
                  placeholder="select date"
                  format="YYYY-MM-DD"
                  minimumDate={moment(selectedLeaveType !== "" && leaveType.find(value => value.id === selectedLeaveType).date_start, "DD/MM/YYYY").toDate()}
                  showIcon={false}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: { position: 'absolute', left: 0, top: 4, marginLeft: 0 },
                    dateInput: { alignItems: 'flex-start', borderWidth: 0, },
                  }}
                  onDateChange={(date) => { this.setState({ startDate: date }) }}
                />
              </Card>
              {/********************* TYPE *******************/}
              <Card style={{ padding: 10, marginBottom: 5, width: '49%', marginRight: 0, marginLeft: 0, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ marginRight: 5 }}>5. Start Time</Text>
                  <Text style={{ color: 'red' }}>*</Text>
                </View>
                <Picker
                  required
                  enabled={selectedLeaveType !== "" ? true : false}
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  headerBackButtonText="back"
                  placeholder="Select Type"
                  selectedValue={selectedStartype}
                  // onValueChange={this.onValueChange.bind(this)}
                  onValueChange={(value) => this.onValueChange(value, 'selectedStartype')}
                  renderHeader={backAction =>
                    <Header style={{ backgroundColor: "#44c1ac" }}>
                      <Left>
                        <Button transparent onPress={backAction}>
                          <Icon name="arrow-back" style={{ color: "#fff" }} />
                        </Button>
                      </Left>
                      <Body style={{ flex: 3 }}>
                        <Title style={{ color: "#fff" }}>Select One</Title>
                      </Body>
                      <Right />
                    </Header>}
                >
                  <Picker.Item label="Select Time" value={null} />
                  {
                    selectedLeaveType !== "" && leaveType.find(value => value.id === selectedLeaveType).time_start.map((item, i) => {
                      {
                        return (<Picker.Item label={item} value={item} />)
                      }
                    })
                  }
                </Picker>
              </Card>
            </View>

            {/********************* END DAY *******************/}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              {/********************* DAY *******************/}
              <Card style={{ padding: 10, marginBottom: 5, width: '49%', marginRight: 0, marginLeft: 0, }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ marginRight: 5 }}>6. End Date</Text>
                  <Text style={{ color: 'red' }}>*</Text>
                </View>
                <DatePicker
                  required
                  disabled={selectedLeaveType !== "" ? false : true}
                  style={{ alignItems: 'flex-start', width: '100%', }}
                  date={endDate}
                  mode="date"
                  hideText={false}
                  placeholder="select date"
                  format="YYYY-MM-DD"
                  minimumDate={moment(startDate, "YYYY-MM-DD").toDate()}
                  showIcon={false}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: { position: 'absolute', left: 0, top: 4, marginLeft: 0 },
                    dateInput: { alignItems: 'flex-start', borderWidth: 0, },
                  }}
                  onDateChange={(date) => { this.setState({ endDate: date }) }}
                />
              </Card>
              {/********************* TYPE *******************/}
              <Card style={{ padding: 10, marginBottom: 5, width: '49%', marginRight: 0, marginLeft: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ marginRight: 5 }}>7. End Time</Text>
                  <Text style={{ color: 'red' }}>*</Text>
                </View>
                <Picker
                  required
                  enabled={selectedLeaveType !== "" ? true : false}
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  headerBackButtonText="back"
                  placeholder="Select Type"
                  selectedValue={selectedEndType}
                  // onValueChange={this.onValueChange.bind(this)}
                  onValueChange={(value) => this.onValueChange(value, 'selectedEndType')}
                  renderHeader={backAction =>
                    <Header style={{ backgroundColor: "#44c1ac" }}>
                      <Left>
                        <Button transparent onPress={backAction}>
                          <Icon name="arrow-back" style={{ color: "#fff" }} />
                        </Button>
                      </Left>
                      <Body style={{ flex: 3 }}>
                        <Title style={{ color: "#fff" }}>Select One</Title>
                      </Body>
                      <Right />
                    </Header>}
                >
                  <Picker.Item label="Select Time" value={null} />
                  {
                    selectedLeaveType !== "" && leaveType.find(value => value.id === selectedLeaveType).time_end.map((item, i) => {
                      {
                        return (<Picker.Item label={item} value={item} />)
                      }
                    })
                  }
                </Picker>
              </Card>
            </View>
            {/********************* REMARK *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>8. Remarks</Text>
              </View>
              <TextInput
                value={remark}
                placeholder="Remarks..."
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'remark')}
                autoCorrect={false}
                autoCapitalize="none"
              // keyboardType={'numeric'}
              />
            </Card>
            {/********************* SUPPORTING DOCUMENT *******************/}
            {/* <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>8. Supporting Document</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              {
                ImageSource === null ? null :
                  <View style={{ width: 100, height: 100, marginTop: 10, }}>
                    <Thumbnail source={ImageSource} style={styles.thumbnailImg} />
                  </View>
              }
              <Button full style={{ marginTop: 10, borderRadius: 8 }} onPress={() => this.takePhoto()}>
                <Text>Add File</Text>
              </Button>
            </Card> */}
            {/********************* APPROVER *******************/}
            {/* <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>Approver(s)</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <Button bordered style={{
                marginTop: 10, width: 60, alignItems: 'center', justifyContent: 'center',
                height: 60, borderRadius: 100, borderColor: '#44c1ac',
              }}>
                <ANT name="plus" size={30} style={styles.addIcon} />
              </Button>
            </Card> */}
            {/********************* CC *******************/}
            {/* <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>CC</Text>
              </View>
              <Button bordered style={{
                marginTop: 10, width: 60, alignItems: 'center', justifyContent: 'center',
                height: 60, borderRadius: 100, borderColor: '#44c1ac',
              }}>
                <ANT name="plus" size={30} style={styles.addIcon} />
              </Button>
            </Card> */}
          </View>
        </ScrollView>
        {/********************* BUTTON SUBMIT *******************/}
        <Button full style={{ backgroundColor: '#44c1ac', height: 60 }} onPress={() => this.onConfirmSubmit()}>
          <Text>SUBMIT</Text>
        </Button>

        <ConfirmDialog
          title="Required"
          visible={this.state.validationMessageModal}
          onTouchOutside={() => this.setState({validationMessageModal: false})}
          positiveButton={{
              title: "Ok",
              onPress: () => this.setState({validationMessageModal: false})
          }}
        >
          <Text>Please fill up the required fields before submit.</Text>
        </ConfirmDialog>
        <ConfirmDialog
            title="Attention"
            visible={this.state.onConfirmSubmitModal}
            onTouchOutside={() => this.setState({onConfirmSubmitModal: false})}
            positiveButton={{
                title: "Submit",
                onPress: () => { this.submitLeave() }
            }}
            negativeButton={{
                title: "Cancel",
                onPress: () => this.setState({onConfirmSubmitModal: false})           
            }} >
            <View style={styles.confirmDialog}>
              <Text>Total leave application: <Text style={{ fontWeight: 'bold' }}>{this.state.onConfirmSubmitModalData.duration} Days</Text></Text>
              <Text style={{ marginTop: 15 }}>Your leave application duration exceeds your leave balance. It will be <Text style={{ fontWeight: 'bold' }}>{this.state.onConfirmSubmitModalData.unpaid_leave} unpaid leave</Text>, if you agree, please tick the following checkbox</Text>
              <View style={{ flexDirection: 'row', marginTop: 15 }}><CheckBox value={this.state.agree_unpaid} onChange={() => { this.setState(prevState => ({ agree_unpaid: !prevState.agree_unpaid })) }} style={styles.confirmDialogChkbox} /><Text style={{ fontWeight: 'bold' }}>Agree to apply unpaid leave</Text></View>
            </View>
        </ConfirmDialog>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  inputsty: { padding: 5, paddingLeft: 0 },
  addIcon: { color: '#44c1ac' },
  thumbnailImg: { width: '100%', height: '100%', borderRadius: 0 },
  confirmDialogChkbox: { position: 'relative', marginTop: -7, marginLeft: -10, marginRight: 5 },
})

export default NewLeaveApplicationScreen;
