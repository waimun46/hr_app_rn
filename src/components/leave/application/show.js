import React, { Component } from 'react';
import { Alert, View, StyleSheet, ScrollView } from 'react-native';
import { Text, Button, Card, CardItem, Body } from 'native-base';
import axios from 'axios';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

class ShowLeaveApplicationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      record: []
    };
  }

  componentDidMount() {
    this.getRecord();
  }

  /*********************************************** getRecord *********************************************/
  getRecord = () => {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/leave_applications/" + this.props.navigation.state.params.id;
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1]
        }
      }

      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((res) => {
          if (res.status === 200) {
            that.setState({
              record: res.data
            })
          }
        })
        .catch(function (error) {
          console.log(error);
        })
    });
  };

  onCancelButton = () => {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/leave_applications/" + this.props.navigation.state.params.id + "/cancel";
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1]
        }
      }

      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((res) => {
          if (res.data.success) {
            Alert.alert('Notice', res.data.success,
              [
                {
                  text: 'OK', onPress: () => { 
                    this.props.navigation.navigate('show_leave_application', { 
                      id: this.props.navigation.state.params.id 
                    }), this.getRecord(); 
                  }
                },
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert('Error', res.data.error,
              [
                {
                  text: 'OK', onPress: () => this.props.navigation.navigate('home')
                },
              ],
              { cancelable: false }
            );
          }
        })
        .catch(function (error) {
          console.log(error);
        })
    });
  };

  render() {
    console.log("this.state.record =====", this.state.record)

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ padding: 10 }}>
            <Card>
              <CardItem>
                <Body>
                  <Text>{this.state.record.leave_category}</Text>
                  <Text>{this.state.record.description}</Text>
                  <Text>{this.state.record.date_start} - {this.state.record.date_end}</Text>
                  <Text>{this.state.record.duration}</Text>
                  <Text>{this.state.record.status}</Text>
                </Body>
              </CardItem>
            </Card>
          </View>
        </ScrollView>
        {
          this.state.record.cancellable && this.state.record.status == 'Submitted' ?
          (
            <Button full danger style={{ height: 60 }} onPress={() => this.onCancelButton()}>
              <Text>CANCEL LEAVE</Text>
            </Button>
          ) : 
          (
            null
          )
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
})

export default ShowLeaveApplicationScreen;