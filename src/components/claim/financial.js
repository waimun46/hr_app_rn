import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, TextInput, ScrollView } from 'react-native';
import { Card, Text, Title, Button, Header, Left, Body, Right, Picker, Icon, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';

class FinancialClaimScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reason: '',
      amount: '',
      ImageSource: null,
      Image_TAG: '',
      ImgData: null,
      isSubmit: false
    };
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'reason') { this.setState({ reason: text }) }
    if (field === 'amount') { this.setState({ amount: text }) }
  }

  /*********************************************** takePhoto *********************************************/
  takePhoto() {
    // console.log('showImagePicker-----')
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        //console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          ImageSource: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          ImgData: response.data
        });
      }
    });
  }


  render() {
    const { reason, amount, startDate, selectedLeaveType, selectedStartype, selectedEndType, duration } = this.state;
    const navigateAction = this.props.navigation.navigate;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ padding: 10, paddingBottom: 30 }}>
            {/********************* AMOUNT *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>1. Amount (RM)</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <TextInput
                value={amount}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'amount')}
                autoCorrect={false}
                autoCapitalize="none"
              // keyboardType={'numeric'}
              />
            </Card>
            {/********************* REASON *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>2. Reason</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <TextInput
                value={reason}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'reason')}
                autoCorrect={false}
                autoCapitalize="none"
              // keyboardType={'numeric'}
              />
            </Card>
          
            {/********************* SUPPORTING DOCUMENT *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>3. Supporting Document</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <Button full style={{ marginTop: 10, borderRadius: 8 }} onPress={() => this.takePhoto()}>
                <Text>Add File</Text>
              </Button>
            </Card>
            {/********************* APPROVER *******************/}
            {/* <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>Approver(s)</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <Button bordered style={{
                marginTop: 10, width: 60, alignItems: 'center', justifyContent: 'center',
                height: 60, borderRadius: 100, borderColor: '#44c1ac',
              }}>
                <ANT name="plus" size={30} style={styles.addIcon} />
              </Button>
            </Card> */}
            {/********************* CC *******************/}
            {/* <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>CC</Text>
              </View>
              <Button bordered style={{
                marginTop: 10, width: 60, alignItems: 'center', justifyContent: 'center',
                height: 60, borderRadius: 100, borderColor: '#44c1ac',
              }}>
                <ANT name="plus" size={30} style={styles.addIcon} />
              </Button>
            </Card> */}
          </View>
        </ScrollView>
        {/********************* BUTTON SUBMIT *******************/}
        <Button full style={{ backgroundColor: '#44c1ac', height: 60 }}>
          <Text>SUBMIT</Text>
        </Button>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  inputsty: { padding: 5, paddingLeft: 0  },
  addIcon: { color: '#44c1ac' }

})

export default FinancialClaimScreen;
