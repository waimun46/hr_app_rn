import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, TextInput, ScrollView } from 'react-native';
import { Card, Text, Title, Button, Header, Left, Body, Right, Picker, Icon, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';

class ChangePasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirm_pass: ''
    };
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'password') { this.setState({ password: text }) }
    if (field === 'confirm_pass') { this.setState({ confirm_pass: text }) }
  }

  render() {
    const { password, confirm_pass } = this.state;
    const navigateAction = this.props.navigation.navigate;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ padding: 10, paddingBottom: 30 }}>
            {/********************* NEW PASSWORD *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>New Password</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <TextInput
                value={password}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'password')}
                autoCorrect={false}
                autoCapitalize="none"
              // keyboardType={'numeric'}
              />
            </Card>
            {/********************* CONFIRM NEW PASSWORD *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>Confirm New Password</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <TextInput
                value={confirm_pass}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'confirm_pass')}
                autoCorrect={false}
                autoCapitalize="none"
              // keyboardType={'numeric'}
              />
            </Card>
          </View>
        </ScrollView>
        {/********************* BUTTON SUBMIT *******************/}
        <Button full style={{ backgroundColor: '#44c1ac', height: 60 }}>
          <Text>SUBMIT</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  inputsty: { padding: 5, paddingLeft: 0 },
  addIcon: { color: '#44c1ac' }

})


export default ChangePasswordScreen;
