import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, TextInput, ScrollView } from 'react-native';
import { Card, Text, Title, Button, Header, Left, Body, Right, Thumbnail, CardItem } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';

const localProfile = "https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png"

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      dob: moment().format('YYYY-MM-DD'),
      email: '',
      phone: '',
      srcImg: null,
      imgUri: '',
      fileName: '',
      imgData: null,
      isSubmit: false,
    };
  }


  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'first_name') { this.setState({ first_name: text }) }
    if (field === 'last_name') { this.setState({ last_name: text }) }
    if (field === 'email') { this.setState({ email: text }) }
    if (field === 'phone') { this.setState({ phone: text }) }
  }

  /*********************************************** takePhoto *********************************************/
  takePhoto() {
    console.log('showImagePicker-----')
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          srcImg: { uri: response.uri },
          imgUri: response.uri,
          fileName: response.fileName,
          imgData: response.data
        });
      }
    });
  }

  render() {
    const { first_name, last_name, dob, srcImg, email, phone, duration } = this.state;
    const navigateAction = this.props.navigation.navigate;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.imageContainer}>
            <TouchableOpacity style={styles.imgWarpProfile} onPress={() => this.takePhoto()}>
              {
                srcImg === null ?
                  <Thumbnail source={{ uri: localProfile }} style={styles.thumbnailImg} />
                  :
                  <Thumbnail source={srcImg} style={styles.thumbnailImg} />
              }
            </TouchableOpacity>
          </View>
          <View style={{ padding: 10, paddingBottom: 30 }}>
            {/********************* FIRST NAME *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>Full Name</Text>
                {/* <Text style={{ color: 'red' }}>*</Text> */}
              </View>
              <View style={{paddingTop: 5, paddingBottom: 5}}>
                <Text style={{ color: '#44c1ac' }}>John</Text>
              </View>
              {/* <TextInput
                value={first_name}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'first_name')}
                autoCorrect={false}
                autoCapitalize="none"
              // keyboardType={'numeric'}
                editable={false} 
              />*/}
            </Card>
            {/********************* LAST NAME *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>Last Name</Text>
              </View>
              <View style={{paddingTop: 5, paddingBottom: 5}}>
                <Text style={{ color: '#44c1ac' }}>Lee</Text>
              </View>
              {/* <TextInput
                value={last_name}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'last_name')}
                autoCorrect={false}
                autoCapitalize="none"
                editable={false}
              // keyboardType={'numeric'}
              /> */}
            </Card>
            {/********************* D.O.B *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>D.O.B</Text>
              </View>
              <View style={{paddingTop: 5, paddingBottom: 5}}>
                <Text style={{ color: '#44c1ac' }}>06-06-1988</Text>
              </View>
              {/* <TextInput
                value={email}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'email')}
                autoCorrect={false}
                autoCapitalize="none"
                editable={false}
              // keyboardType={'numeric'}
              /> */}
              {/* <DatePicker
                style={{ alignItems: 'flex-start', width: '100%', }}
                date={dob}
                mode="date"
                hideText={false}
                placeholder="select date"
                format="YYYY-MM-DD"
                // minDate="2016-05-01"
                // maxDate="2016-06-01"
                showIcon={false}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: { position: 'absolute', left: 0, top: 4, marginLeft: 0 },
                  dateInput: { alignItems: 'flex-start', borderWidth: 0, },
                }}
                onDateChange={(date) => { this.setState({ dob: date }) }}
              /> */}
            </Card>
            {/********************* EMAIL ADDRESS *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>Email Address</Text>
              </View>
              <View style={{paddingTop: 5, paddingBottom: 5}}>
                <Text style={{ color: '#44c1ac' }}>john@gmail.com</Text>
              </View>
              {/* <TextInput
                value={email}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'email')}
                autoCorrect={false}
                autoCapitalize="none"
                editable={false}
              // keyboardType={'numeric'}
              /> */}
            </Card>
            {/********************* PHONE NUMBER *******************/}
            <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>Phone Number</Text>
              </View>
              <View style={{paddingTop: 5, paddingBottom: 5}}>
                <Text style={{ color: '#44c1ac' }}>0123456789</Text>
              </View>
              {/* <TextInput
                value={phone}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'phone')}
                autoCorrect={false}
                autoCapitalize="none"
                editable={false}
              // keyboardType={'numeric'}
              /> */}
            </Card>
            {/********************* CHANGE PASSWORD *******************/}
            <Card style={{ padding: 10, marginBottom: 5, height: 60, justifyContent: 'center' }}>
              <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between', }} onPress={() => navigateAction('change_pass')}>
                <View style={{flexDirection: 'row', justifyContent: 'center',alignItems: 'center' }}>
                  <ANT name="lock1" size={25} style={[styles.addIcon, {marginRight: 5}]} />
                  <Text style={{ marginRight: 5 }}>Change Password</Text>
                </View>
                <ANT name="right" size={20} style={[styles.addIcon, {color: '#ccc'}]} />
              </TouchableOpacity>
            </Card>
          </View>
        </ScrollView>
        {/********************* BUTTON SUBMIT *******************/}
        <Button full style={{ backgroundColor: '#44c1ac' , height: 60}}>
          <Text>SUBMIT</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  inputsty: { padding: 5, paddingLeft: 0 },
  addIcon: { color: '#44c1ac' },
  headerImg: { position: 'relative', marginBottom: 50, justifyContent: 'center', backgroundColor: 'transparent' },
  imgWarpProfile: {
    width: '100%', alignItems: 'center', borderWidth: .5, width: 120, height: 120, borderRadius: 100,
    padding: 5, borderColor: '#fff',
  },
  thumbnailImg: { width: '100%', height: '100%', borderRadius: 100, },
  imageContainer: { height: 170, alignItems: 'center', justifyContent: 'center', backgroundColor: '#44c1ac' },
})

export default ProfileScreen;
