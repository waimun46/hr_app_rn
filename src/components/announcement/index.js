import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, FlatList, ScrollView } from 'react-native';
import { Card, Text, Thumbnail, List, ListItem, Left, Body, Right, Button } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';




const data = [
  {
    type: 'in', time: '10:26:00 PM', status: 'Clock In', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
  {
    type: 'out', time: '10:26:00 PM', status: 'Clock Out', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
]

class AnnouncementScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*********************************************** _renderHistory *********************************************/
  _renderHistory = ({ item }) => {
    return (
      <List>
        <ListItem thumbnail>
          <Left>
            <ANT name="infocirlce" size={30} style={{ color: '#44c1ac' }} />
          </Left>
          <Body>
            <Text>Ms Kelly Tan</Text>
            <Text note numberOfLines={1}>MCO work from home...</Text>
          </Body>
          <Right>
            <Right>
              <Text note>2 week ago</Text>
            </Right>
          </Right>
        </ListItem>
      </List>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          {/******************************** ATTENDANCE HISTORY ******************************* */}
          <FlatList
            data={data}
            extraData={this.state}
            renderItem={this._renderHistory}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ marginTop: 10, paddingBottom: 50 }}
          />
        </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },



})

export default AnnouncementScreen;
