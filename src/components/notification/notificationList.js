import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, FlatList, ScrollView } from 'react-native';
import { Card, Text, Thumbnail, List, ListItem, Left, Body, Right, Button } from 'native-base';
import axios from 'axios';

class NotificationListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: []
    };
  }

  componentDidMount(){
    this.getNotificationList()
  }
    /*********************************************** getNotification *********************************************/
    getNotificationList() {
      const userInfo = this.props.navigation.state.params;
      let baseIp = `https://erp-ice.com/api/v1/notifications/${userInfo.dataNoti.id}/read`;
      let axiosConfig = {
        headers: {
          'access-token': userInfo.accessToken,
          'client': userInfo.clientId,
          'uid': userInfo.uidId
        }
      };
      console.log('dsss', userInfo.accessToken,userInfo.clientId,userInfo.uidId)
      console.log('fetchData----baseIp', baseIp);
      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((fetchData) => {
          console.log('fetchData----baseIp', baseIp);
          if (fetchData.status === 200) {
            that.setState({
              listData: fetchData.data
            })
          }
        })
        .catch(function (error) {
          console.log(error);
        })
    }

  render() {
    const {listData} = this.state;
    console.log('props----dataNoti-----ss', listData)

    return (
      <View style={styles.container}>
        <ScrollView>
          
          <Card style={{padding: 20, marginTop: 20}}>
            <Text style={{color: '#44c1ac', fontWeight: 'bold'}}>{listData.user_from}</Text>
            <Text style={{marginTop:10, marginBottom :10}}>{listData.message === "" ? 'You have no message' : listData.message }</Text>
            <Text note style={{textAlign: 'right'}}>{listData.created_at}</Text>
          </Card>

        </ScrollView>
      </View>

    );
  }
}
const styles = StyleSheet.create({
  container: { flex: 1,  },



})
export default NotificationListScreen;
