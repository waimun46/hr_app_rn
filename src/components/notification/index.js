import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, FlatList, ScrollView } from 'react-native';
import { Card, Text, Thumbnail, List, ListItem, Left, Body, Right, Button } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';


class NotificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: '',
      clientId: '',
      uidId: '',
      notificationData: []
    };
  }

  componentDidMount() {
    this.getNotification();
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      this.getNotification();
    });
  }

  /*********************************************** getNotification *********************************************/
  getNotification = () => {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/notifications";
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1],
        }
      }

      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((response) => {
          if (response.status === 200) {
            that.setState({
              notificationData: response.data.notifications
            })
          }
        })
        .catch(function (error) {
          console.log(error);
        })  
    })
  };

  onReadNotification = (id) => {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/notifications/" + id + "/read";
      let axiosConfig = {
          headers: {
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1],
        }
      }

      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((response) => {
          console.log("CHECK RESPONSE NOTIFICATIONS ===> ", response)
          if (response.status === 200) {
            if (response.data.target_type == "Approval") {
              this.props.navigation.navigate('show_leave_approval', { id: response.data.target_id })
            } else if (response.data.target_type == "LeaveApplication") {
              this.props.navigation.navigate('show_leave_application', { id: response.data.target_id })
            }
          }
        })
        .catch(function (error) {
          console.log(error);
        })  
    })
  };

  /*********************************************** _renderHistory *********************************************/
  _renderHistory = ({ item }) => {
    const navigateAction = this.props.navigation.navigate;
    const {accessToken,clientId,uidId} = this.state;
    return (
      <List style={{ backgroundColor: item.read == false ? '#E6EAEF' : null }}>
        <ListItem thumbnail onPress={() => this.onReadNotification(item.id) }>
          <Left>
            <MCI name="bell" size={30} style={{ color: '#44c1ac' }} />
          </Left>
          <Body>
            <Text /* numberOfLines={1} */ style={{ marginTop: 5 }}>{item.message}</Text>
            <Text note style={{ marginTop: 5 }}>{item.created_at}</Text>
          </Body>
          {/* <Right>
            <Right>
              <Text note>{item.target_type}</Text>
            </Right>
          </Right> */}
        </ListItem>
      </List>
    )
  }

  render() {
    const { notificationData } = this.state;
    console.log('notificationData---', notificationData)
    return (
      <View style={styles.container}>
        <ScrollView>
          {/******************************** ATTENDANCE HISTORY ******************************* */}
          {
            notificationData.length > 0 ?
            (<FlatList
              data={notificationData}
              extraData={this.state}
              renderItem={this._renderHistory}
              keyExtractor={(item, index) => index.toString()}
              // contentContainerStyle={{ marginTop: 10, paddingBottom: 50 }}
            />)
            :
            (<Text>No Notification</Text>)
          }
        </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
})

export default NotificationScreen;
