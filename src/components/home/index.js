import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, ActivityIndicator, Image, FlatList, ScrollView, Modal } from 'react-native';
import { Card, Text, Thumbnail, Button } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import RNRestart from 'react-native-restart';
import Geolocation from '@react-native-community/geolocation';
import { getDistance, getPreciseDistance } from 'geolib';


const logo = "https://ya-webdesign.com/images250_/instagram-circle-png-2.png"

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: '',
      clientId: '',
      uidId: '',
      profile: [],
      statusclock: null,
      unreadNotificationCount: 0,
      isLoginVisible: true,
      username: '',
      password: '',
      isSubmit: false,
      latitude: null,
      longitude: null,
    };
  }
  componentDidMount() {
    /************************************  AsyncStorage ACCESS_TOKEN ***************************************/
    AsyncStorage.getItem("ACCESS_TOKEN").then(accessTokenAsynRes => {
      this.setState({
        accessToken: accessTokenAsynRes
      })
    })
    /************************************  AsyncStorage CLIENT ***************************************/
    AsyncStorage.getItem("CLIENT").then(clientIdAsynRes => {
      this.setState({
        clientId: clientIdAsynRes
      })
    })
    /************************************  AsyncStorage UID ***************************************/
    AsyncStorage.getItem("UID").then(uidIdAsynRes => {
      this.setState({
        uidId: uidIdAsynRes
      })
    })
    /************************************  AsyncStorage PROFILE_DATA ***************************************/
    AsyncStorage.getItem("PROFILE_DATA").then(profileAsynRes => {
      this.setState({
        profile: JSON.parse(profileAsynRes)
      })
    })
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      // console.log('componentDidMount----fetchGalleryDataListTest');
      AsyncStorage.getItem("CLOCKIN_STATUS").then(clockinAsynRes => {
        this.setState({
          statusclock: clockinAsynRes
        })
      })
      this.getUserClockInDStatus();
      this.getUnreadNotificationCount();
    });

    this.getLocation()
  }

  /*********************************************** getLocation *********************************************/
  getLocation() {
    Geolocation.getCurrentPosition(
      (position) => {
        this.setState(({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        })
        );
      },
      (error) => this.setState({ forecast: error.message }),
      { enableHighAccuracy: true, timeout: 500000 },
    );
  }

  /*********************************************** getLeaveType *********************************************/
  getUserClockInDStatus() {
    // console.log('getUserClockInDStatus--')
    const token = this.state.accessToken;
    const client = this.state.clientId;
    const uid = this.state.uidId;
    let baseIp = "https://erp-ice.com/api/v1/attendances/has_clock_in";
    let axiosConfig = {
      headers: {
        'access-token': token,
        'client': client,
        'uid': uid
      }
    };
    let that = this;
    axios.get(baseIp, axiosConfig)
      .then((fetchData) => {
        // console.log('fetchData----statusUserClockIn', fetchData);
        if (fetchData.status === 200) {
          that.setState({
            statusUserClockIn: fetchData.data
          })
        }
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  getUnreadNotificationCount = () => {
    AsyncStorage.multiGet(["ACCESS_TOKEN", "CLIENT", "UID"]).then(response => {
      let baseIp = "https://erp-ice.com/api/v1/notifications";
      let axiosConfig = {
        headers: {
          'Cache-Control': 'no-cache',
          'access-token': response[0][1],
          'client': response[1][1],
          'uid': response[2][1],
        }
      }

      let that = this;
      axios.get(baseIp, axiosConfig)
        .then((response) => {
          if (response.status === 200) {
            that.setState({
              unreadNotificationCount: response.data.unread_count
            })
          }
        })
        .catch(function (error) {
          console.log(error);
        })
    })
  };

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'username') { this.setState({ username: text }) }
    if (field === 'password') { this.setState({ password: text }) }
  }


  onSubmit() {
    // const navigateAction = this.props.navigation.navigate;
    let baseIp = "https://erp-ice.com/api/v1/auth/sign_in";
    let outputJson = [
      {
        username: this.state.username,
        password: this.state.password
      }
    ]
    this.setState({
      isSubmit: true
    })
    let that = this;
    let dis = getDistance(
      { latitude: this.state.latitude, longitude: this.state.longitude },
      { latitude: 3.150259, longitude: 101.707148 }
    );
    console.log('outputJson---dis', outputJson, dis)

    axios.post(baseIp, {
      username: this.state.username, password: this.state.password,
    })
      .then((res) => {
        // console.log('login----',JSON.stringify(res));
        if (res.status === 200) {
          that.setState({
            isSubmit: false,
            isLoginVisible: false
          })
          AsyncStorage.setItem('ACCESS_TOKEN', res.headers['access-token']);
          AsyncStorage.setItem('CLIENT', res.headers['client']);
          AsyncStorage.setItem('UID', res.headers['uid']);
          AsyncStorage.setItem('PROFILE_DATA', JSON.stringify(res.data));
          // return navigateAction('home')
          RNRestart.Restart();
        }
      })
      .catch(function (err) {
        alert("Login Invalid", err);
        that.setState({
          isSubmit: false
        })
      });
  }


  render() {
    const { accessToken, clientId, uidId, statusclock, profile, isLoginVisible, username,
      password, isSubmit , latitude, longitude} = this.state;
    const navigateAction = this.props.navigation.navigate;
    const dataProfile = profile.data || {}
    console.log('home-----', accessToken, clientId, uidId)
    // console.log('status-----', statusclock)
    // console.log('profile------', accessToken)
    console.log('latitude,longitude------', latitude, longitude)

    return (
      accessToken === null ?
        <Modal
          transparent={false}
          animationType={'none'}
          visible={isLoginVisible}
          onRequestClose={() => { console.log('close modal') }}>
          <View style={styles.containerLogin}>
            <ScrollView style={{ width: '100%', }}>
              {/*************************************** submit loading ****************************************/}
              {
                isSubmit ?
                  <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={isSubmit}
                    onRequestClose={() => { console.log('close modal') }}>
                    <View style={styles.modalBackground}>
                      <View style={styles.activityIndicatorWrapper}>
                        <ActivityIndicator color="primary" size="large" animating={isSubmit} />
                      </View>
                    </View>
                  </Modal>
                  : null
              }
              <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
                <View style={{ marginBottom: 30, width: 300, height: 80, alignItems: 'center' }}>
                  <Image source={require('../../assets/images/ICE-Logo.png')}
                    style={{ width: '100%', height: '100%', resizeMode: 'contain' }}
                  />
                </View>
                <Text style={styles.logoName}>Ice Holidays</Text>
                <View style={{ width: '80%', alignContent: 'center' }}>
                  <View style={{ marginBottom: 20 }}>
                    <Text>Username :</Text>
                    <TextInput
                      value={username}
                      placeholder="Fill in your username"
                      placeholderTextColor="#8c8c8c"
                      style={styles.inputsty}
                      onChangeText={(text) => this.onChangeTextInput(text, 'username')}
                      autoCorrect={false}
                      autoCapitalize="none"
                    // keyboardType={'numeric'}
                    />
                  </View>
                  <View style={{ marginBottom: 30 }}>
                    <Text>Password :</Text>
                    <TextInput
                      value={password}
                      placeholder="Fill in your password"
                      placeholderTextColor="#8c8c8c"
                      style={styles.inputsty}
                      onChangeText={(text) => this.onChangeTextInput(text, 'password')}
                      autoCorrect={false}
                      autoCapitalize="none"
                      // keyboardType={'numeric'}
                      secureTextEntry={true}
                    />
                  </View>
                  <Button full onPress={() => this.onSubmit()} style={{ borderRadius: 8 }}>
                    <Text style={{ color: '#fff' }}>Login</Text>
                  </Button>
                  <Button full transparent onPress={() => Linking.openURL('https://erp-ice.com/users/password/new')}>
                    <Text>Forgot your password?</Text>
                  </Button>

                </View>
              </View>
            </ScrollView>
          </View>
        </Modal>
        :
        <ScrollView>
          <View style={styles.container}>
            {/******************************** TOP HEADER ******************************* */}
            <View style={styles.backgroundSty}>
              <View style={styles.logoContainer}>
                <Text style={{ fontSize: RFPercentage('4'), fontWeight: 'bold', color: '#fff', }}>
                  Ice Holidays
              </Text>
                {/* <View style={styles.logoShape}>
                <Image source={{ uri: logo }} style={styles.logoImg} />
              </View> */}
              </View>
              <View >
                <Text style={styles.subTitle}>Good Day, {dataProfile.full_name} What to you want to do today?</Text>
              </View>
            </View>
          </View>

          {/******************************** CLOCK IN ******************************* */}
          <View style={styles.clockInContainer}>
            <Card style={styles.clockContent}>
              <View style={{ width: '70%' }}>
                <Text style={{ fontSize: RFPercentage('3.5') }}>Clock In</Text>
                <Text style={{ marginTop: 8 }}>You haven't clock in yet.</Text>
                <View style={{ flexDirection: 'row', marginTop: 12 }}>
                  <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontWeight: 'bold', color: '#44c1ac' }}>Have a nice days...</Text>
                    <ANT name="export" size={25} style={styles.iconClock} />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ width: '30%' }}>
                <TouchableOpacity onPress={() => navigateAction('attendance', {
                  access_token: accessToken,
                  client: clientId,
                  uid: uidId,
                  clockInStatus: statusclock
                })}>
                  <View style={[styles.clockShape, { borderColor: statusclock === null ? '#44c1ac' : 'red', }]}>
                    <View style={[styles.clockInBtn, { backgroundColor: statusclock === null ? '#44c1ac' : 'red', }]}>
                      {
                        statusclock === '1' ?
                          <Text style={{ color: '#fff' }}>Clock Out</Text>
                          :
                          <Text style={{ color: '#fff' }}>Clock In</Text>
                      }

                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </Card>
          </View>

          {/******************************** SELECT BUTTON ******************************* */}
          <View style={styles.selectContainer}>
            <Card style={styles.selectContent}>
              {/* <View style={styles.selectBtnRow}>
              <TouchableOpacity style={styles.selectBtnWidth}>
                <ANT name="gift" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>REWARD</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.selectBtnWidth}>
                <ANT name="Trophy" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>POLL</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.selectBtnWidth}>
                <ANT name="solution1" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>JOB</Text>
              </TouchableOpacity>
            </View> */}
              <View style={[styles.selectBtnRow, { marginTop: 0 }]}>
                {/* <TouchableOpacity style={styles.selectBtnWidth} onPress={() => navigateAction('claim')}>
                <ANT name="calculator" size={40} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>CLAIM</Text>
              </TouchableOpacity> */}
                <TouchableOpacity style={styles.selectBtnWidth} onPress={() => navigateAction('leave',
                  {
                    access_token: accessToken,
                    client: clientId,
                    uid: uidId,
                    clockInStatus: statusclock
                  }
                )}>
                  <ANT name="table" size={40} style={{ color: '#44c1ac' }} />
                  <Text note style={styles.selectText}>LEAVE</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.selectBtnWidth}
                  onPress={() => navigateAction('attendance')}>
                  <ANT name="barchart" size={40} style={{ color: '#44c1ac' }} />
                  <Text note style={styles.selectText}>ATTENDANCE</Text>
                </TouchableOpacity>
              </View>
              {/* <View style={[styles.selectBtnRow, { marginTop: 30 }]}>
              <TouchableOpacity style={styles.selectBtnWidth}>
                <ANT name="form" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>FEEDBACK</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.selectBtnWidth}>
                <ANT name="dashboard" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>MILEAGE</Text>
              </TouchableOpacity>
                  <TouchableOpacity style={styles.selectBtnWidth}>
                <ANT name="filetext1" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>APPROVAL</Text>
              </TouchableOpacity>
            </View> */}
              {/* <View style={[styles.selectBtnRow, { marginTop: 30 }]}>
              <TouchableOpacity style={styles.selectBtnWidth}>
                <ANT name="calendar" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>CALENDER</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.selectBtnWidth}>
                <ANT name="folderopen" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>DOCUMENTS</Text>
              </TouchableOpacity>
              <View style={styles.selectBtnWidth}>
                <ANT name="solution1" size={50} style={{ color: '#44c1ac' }} />
                <Text note style={styles.selectText}>LEAVE</Text>
              </View>
            </View> */}
            </Card>
          </View>



        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  backgroundSty: { backgroundColor: '#44c1ac', height: 160, padding: 20, },
  logoContainer: { paddingRight: 5, },
  logoShape: {
    borderRadius: 100, backgroundColor: '#44c1ac',
    width: 80, height: 80, padding: 5, borderWidth: 1, borderColor: '#FFF',
  },
  logoImg: { width: '100%', height: '100%', resizeMode: 'contain', borderRadius: 100 },
  subTitle: { fontSize: RFPercentage('2.3'), marginTop: 10, fontWeight: 'bold' },
  clockShape: {
    borderRadius: 100, width: 100, height: 100, padding: 5, borderWidth: 1,

  },
  iconClock: { paddingLeft: 10, color: '#44c1ac' },
  clockInContainer: { paddingLeft: 20, paddingRight: 20, marginTop: -35, width: '100%', },
  clockContent: { height: 140, borderRadius: 20, padding: 20, flexDirection: 'row', },
  clockInBtn: {
    width: '100%', height: '100%', borderRadius: 100,
    alignItems: 'center', justifyContent: 'center'
  },
  selectContainer: { paddingLeft: 20, paddingRight: 20, width: '100%', marginTop: 10 },
  selectContent: { borderRadius: 20, padding: 20, },
  selectText: { marginTop: 10, color: '#000' },
  selectBtnRow: { flexDirection: 'row', },
  selectBtnWidth: { width: '33.33%', alignItems: 'center' },




  containerLogin: { flex: 1, backgroundColor: '#44c1ac', },
  inputsty: { padding: 10, backgroundColor: '#fff', marginTop: 10, borderRadius: 5 },
  logoName: { fontSize: RFPercentage('4'), fontWeight: 'bold', color: '#fff', marginTop: -10, marginBottom: 20 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },

})


export default HomeScreen;
