import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, TextInput, Image, Modal, ScrollView , ActivityIndicator, Linking} from 'react-native';
import { Card, Text, Thumbnail, Button } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import API from '../../api';


class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isSubmit: false
    };
  }

  onSubmit() {
    // const navigateAction = this.props.navigation.navigate;
    let baseIp = "https://erp-ice.com/api/v1/auth/sign_in";
    let outputJson = [
      {
        username: this.state.username,
        password: this.state.password
      }
    ]
    this.setState({
      isSubmit: true
    })
    let that = this;
    console.log('outputJson---', outputJson)
    axios.post(baseIp, {
      username: this.state.username, password: this.state.password,
    })
      .then((res) => {
        // console.log('login----',JSON.stringify(res));
        if (res.status === 200) {
          that.setState({
            isSubmit: false
          })
          AsyncStorage.setItem('ACCESS_TOKEN', res.headers['access-token']);
          AsyncStorage.setItem('CLIENT', res.headers['client']);
          AsyncStorage.setItem('UID', res.headers['uid']);
          AsyncStorage.setItem('PROFILE_DATA', JSON.stringify(res.data));
          // return navigateAction('home')
        }
      })
      .catch(function (err) {
        alert("Login Invalid", err);
        that.setState({
          isSubmit: false
        })
      });
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'username') { this.setState({ username: text }) }
    if (field === 'password') { this.setState({ password: text }) }
  }


  render() {
    const { username, password, isSubmit } = this.state;
    // const navigateAction = this.props.navigation.navigate;
    return (

      <View style={styles.container}>
        <ScrollView style={{ width: '100%', }}>
          {/*************************************** submit loading ****************************************/}
          {
            isSubmit ?
              <Modal
                transparent={true}
                animationType={'none'}
                visible={isSubmit}
                onRequestClose={() => { console.log('close modal') }}>
                <View style={styles.modalBackground}>
                  <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator color="primary" size="large" animating={isSubmit} />
                  </View>
                </View>
              </Modal>
              : null
          }
          <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
            <View style={{ marginBottom: 30, width: 300, height: 80, alignItems: 'center' }}>
              <Image source={require('../../assets/images/ICE-Logo.png')}
                style={{ width: '100%', height: '100%', resizeMode: 'contain' }}
              />
            </View>
            <Text style={styles.logoName}>Ice Holidays</Text>
            <View style={{ width: '80%', alignContent: 'center' }}>
              <View style={{ marginBottom: 20 }}>
                <Text>Username :</Text>
                <TextInput
                  value={username}
                  placeholder="Fill in your username"
                  placeholderTextColor="#8c8c8c"
                  style={styles.inputsty}
                  onChangeText={(text) => this.onChangeTextInput(text, 'username')}
                  autoCorrect={false}
                  autoCapitalize="none"
                // keyboardType={'numeric'}
                />
              </View>
              <View style={{ marginBottom: 30 }}>
                <Text>Password :</Text>
                <TextInput
                  value={password}
                  placeholder="Fill in your password"
                  placeholderTextColor="#8c8c8c"
                  style={styles.inputsty}
                  onChangeText={(text) => this.onChangeTextInput(text, 'password')}
                  autoCorrect={false}
                  autoCapitalize="none"
                  // keyboardType={'numeric'}
                  secureTextEntry={true}
                />
              </View>
              <Button full onPress={() => this.onSubmit()} style={{ borderRadius: 8 }}>
                <Text style={{ color: '#fff' }}>Login</Text>
              </Button>
              <Button full transparent onPress={() => Linking.openURL('https://erp-ice.com/users/password/new')}>
                <Text>Forgot your password?</Text>
              </Button>

            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#44c1ac', },
  inputsty: { padding: 10, backgroundColor: '#fff', marginTop: 10, borderRadius: 5 },
  logoName: { fontSize: RFPercentage('4'), fontWeight: 'bold', color: '#fff', marginTop: -10, marginBottom: 20 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },


})


export default LoginScreen;
