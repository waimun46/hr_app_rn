import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, FlatList, ScrollView } from 'react-native';
import { Card, Text, Thumbnail, List, ListItem, Left, Body, Right, Item, } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import DatePicker from 'react-native-datepicker';
import ProgressCircle from 'react-native-progress-circle';
import ANT from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

const logo = "https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png";

const data = [
  {
    type: 'in', time: '10:26:00 PM', status: 'Clock In', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
  {
    type: 'out', time: '10:26:00 PM', status: 'Clock Out', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
  {
    type: 'in', time: '10:26:00 PM', status: 'Clock In', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
  {
    type: 'out', time: '10:26:00 PM', status: 'Clock Out', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
]

const progress = [
  { type: 'Present', percent: '95' },
  { type: 'On.Time', percent: '90' },
  { type: 'Working Hours', percent: '90' },
  { type: 'Complate Working', percent: '100' },
]

class AttendanceRecordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: moment().format('DD-MM-YYYY'),
      endDate: moment().format('DD-MM-YYYY'),

    };
  }

  _renderTop = ({ item }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <Card style={styles.cardContainer}>
        {/******************************** TITLE TYPE ******************************* */}
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={[styles.statusBtn, {
            marginRight: 5,
            backgroundColor:
              item.type === 'Present' ? "#44c1ac"
                : item.type === 'On.Time' ? "#3399FF"
                  : item.type === 'Working Hours' ? "red"
                    : '#f3a91ec7'
          }]} />
          <Text note numberOfLines={1} style={{ color: '#000' }}>{item.type}</Text>
        </View>

        {/******************************** PROGRESS CHART ******************************* */}
        <View style={{ marginTop: 10 }}>
          <ProgressCircle
            percent={item.percent}
            radius={50}
            borderWidth={8}
            color={
              item.type === 'Present' ? "#44c1ac"
                : item.type === 'On.Time' ? "#3399FF"
                  : item.type === 'Working Hours' ? "red"
                    : '#f3a91ec7'
            }
            shadowColor="#eee"
            bgColor="#fff"
          >
            <Text style={{ fontSize: 18 }}>{item.percent}%</Text>
          </ProgressCircle>
        </View>
      </Card>
    )
  }

  /*********************************************** _renderHistory *********************************************/
  _renderHistory = ({ item }) => {
    return (
      <List>
        <ListItem avatar>
          <Left>
            <View style={[styles.statusBtn, { backgroundColor: item.type === 'in' ? 'green' : 'red', }]} />
          </Left>
          <Body>
            <Text>{item.status} - {item.time}</Text>
            <Text note numberOfLines={1} style={{ color: 'gray', marginTop: 5 }}>{item.location}</Text>
          </Body>
        </ListItem>
      </List>
    )
  }


  render() {
    return (
      <View style={styles.container}>
        <ScrollView>

          <View style={styles.backgroundSty}>
            {/******************************** TOP HEADER ******************************* */}
            <View style={styles.logoContainer}>
              <View style={styles.logoShape}>
                <Image source={{ uri: logo }} style={styles.logoImg} />
              </View>
            </View>
            <View style={{ width: '60%', }}>
              {/******************************** START DATE ******************************* */}
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <DatePicker
                  style={{ alignItems: 'flex-start', width: 35 }}
                  date={this.state.startDate}
                  mode="date"
                  hideText={true}
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  // minDate="2016-05-01"
                  // maxDate="2016-06-01"
                  showIcon={true}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: { position: 'absolute', left: 0, top: 4, marginLeft: 0 },
                    dateInput: { alignItems: 'flex-start', borderWidth: 0 },
                  }}
                  onDateChange={(date) => { this.setState({ startDate: date }) }}
                />
                <Text>Start Date:  {this.state.startDate}</Text>
              </View>

              {/******************************** END DATE ******************************* */}
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                <DatePicker
                  style={{ alignItems: 'flex-start', width: 35 }}
                  date={this.state.endDate}
                  mode="date"
                  hideText={true}
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  // minDate="2016-05-01"
                  // maxDate="2016-06-01"
                  showIcon={true}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: { position: 'absolute', left: 0, top: 4, marginLeft: 0 },
                    dateInput: { alignItems: 'flex-start', borderWidth: 0 },
                  }}
                  onDateChange={(date) => { this.setState({ endDate: date }) }}
                />
                <Text>End Date:  {this.state.endDate}</Text>
              </View>
            </View>
          </View>

          {/******************************** TYPE CHART ******************************* */}
          <View style={{marginTop: -40}}>
            <FlatList
              data={progress}
              horizontal={true}
              renderItem={this._renderTop}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          {/******************************** ATTENDANCE HISTORY ******************************* */}
          <FlatList
            data={data}
            extraData={this.state}
            renderItem={this._renderHistory}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ marginTop: 20, paddingBottom: 50 }}
          />

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
  backgroundSty: { backgroundColor: '#44c1ac', height: 180, flexDirection: 'row', padding: 20, },
  logoContainer: { width: '30%', alignItems: 'center', paddingRight: 5, },
  logoShape: {
    borderRadius: 100, backgroundColor: '#44c1ac',
    width: 100, height: 100, padding: 5, borderWidth: 1, borderColor: '#FFF',
  },
  logoImg: { width: '100%', height: '100%', resizeMode: 'contain', borderRadius: 100 },
  subTitle: { fontSize: RFPercentage('3.7'), marginTop: 10, fontWeight: 'bold' },
  statusBtn: { width: 8, height: 8, borderRadius: 50 },
  cardContainer: { padding: 15, marginRight: 10, width: 130, alignItems: 'center' },


})

export default AttendanceRecordScreen;
