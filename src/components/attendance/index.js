import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Modal, Image, FlatList, ScrollView, TextInput, ActivityIndicator } from 'react-native';
import { Card, Text, Thumbnail, List, ListItem, Left, Body, Right, Button } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ANT from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import Geolocation from '@react-native-community/geolocation';
import RNFetchBlob from 'rn-fetch-blob';
import AsyncStorage from '@react-native-community/async-storage';

const data = [
  {
    type: 'in', time: '10:26:00 PM', status: 'Clock In', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
  {
    type: 'out', time: '10:26:00 PM', status: 'Clock Out', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
  {
    type: 'in', time: '10:26:00 PM', status: 'Clock In', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
  {
    type: 'out', time: '10:26:00 PM', status: 'Clock Out', location: 'Jalan Kenari 2, Banday Puchong Jaya, 47170 Selangor'
  },
]

class AttendanceScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: '',
      date: '',
      remark: '',
      device: '',
      srcImgClockIn: null,
      imgUriClockIn: '',
      fileNameClockIn: '',
      imgDataClockIn: null,
      srcImgClockOut: null,
      imgUriClockOut: '',
      fileNameClockOut: '',
      imgDataClockOut: null,
      isSubmit: false,
      latitude: null,
      longitude: null,
      clockInStatus: false,
      statusUserClockIn: []

    };
  }

  /*********************************************** componentDidMount *********************************************/
  componentDidMount() {
    this.intervalID = setInterval(
      () => this.tick(),
      1000
    );
    this.getLocation()
   
  }

  /*********************************************** componentWillUnmount *********************************************/
  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  /*********************************************** tick *********************************************/
  tick() {
    this.setState({
      time: moment().format('hh:mm:ss a'),
      date: moment().format('DD-MM-YYYY')
    });
  }

  /*********************************************** getLocation *********************************************/
  getLocation() {
    Geolocation.getCurrentPosition(
      (position) => {
        this.setState(({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        })
        );
      },
      (error) => this.setState({ forecast: error.message }),
      { enableHighAccuracy: true, timeout: 500000 },
    );
  }

  /*********************************************** takePhotoClockIn *********************************************/
  takePhotoClockIn() {
    console.log('showImagePicker-----')
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          srcImgClockIn: { uri: response.uri },
          imgUriClockIn: response.uri,
          fileNameClockIn: response.fileName,
          imgDataClockIn: response.data
        });
      }
    });
  }

  /*********************************************** takePhotoClockOut *********************************************/
  takePhotoClockOut() {
    console.log('showImagePicker-----')
    const options = {
      title: 'Select Photo',
      maxWidth: 1000,
      maxHeight: 1000,
      quality: 0.7,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        //console.log('User cancelled image picker');
      }
      else if (response.error) {
        //console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      }
      else {
        //console.log(response.fileName);
        //console.log(response, '----------response');
        this.setState({
          srcImgClockOut: { uri: response.uri },
          imgUriClockOut: response.uri,
          fileNameClockOut: response.fileName,
          imgDataClockOut: response.data
        });
      }
    });
  }



  /*********************************************** clockIn *********************************************/
  clockIn() {
    const navigateAction = this.props.navigation.navigate;
    console.log('clockIn------------------------------');
    const { device, remark, imgDataClockIn, latitude, longitude } = this.state;
    const propsData = this.props.navigation.state.params;
    const token = propsData.access_token;
    const client = propsData.client;
    const uid = propsData.uid;

    this.setState({
      isSubmit: true
    })

    // console.log('props------', token, client, uid)
    let outputJson = {
      location: `(${latitude}${','}${longitude})`,
      remark: remark,
      device: device,
      clocked_in_image: imgDataClockIn,
    }
    // console.log('outputJson----', outputJson)
    let baseIp = "https://erp-ice.com/api/v1/attendances";
    let postData = {
      attendance: {
        location: `(${latitude}${','}${longitude})`,
        remark: remark,
        device: device,
        clocked_in_image: imgDataClockIn
      }
    };
    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json',
        'access-token': token,
        'client': client,
        'uid': uid
      }
    };
    axios.post(baseIp, postData, axiosConfig)
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            clockInStatus: true,
            isSubmit: false,
            remark: '',
            device: '',
            srcImgClockIn: null
          })
          AsyncStorage.setItem('CLOCKIN_STATUS', '1');
          navigateAction('home')
        } else {
          this.setState({
            clockInStatus: false,
            isSubmit: false,
            remark: '',
            device: '',
            srcImgClockIn: null
          })
        }
        console.log("RESPONSE RECEIVED: clock in ", res);
      })
      .catch((err) => {
        console.log("AXIOS ERROR: ", err);
      })
  }

  /*********************************************** clockOut *********************************************/
  clockOut() {
    console.log('clockout----------------------');
    const navigateAction = this.props.navigation.navigate;
    const { device, remark, imgDataClockOut, latitude, longitude, srcImgClockOut } = this.state;
    const propsData = this.props.navigation.state.params;
    const token = propsData.access_token;
    const client = propsData.client;
    const uid = propsData.uid;
    // console.log('props------', token, client, uid)

    this.setState({
      isSubmit: true
    })

    let outputJson = {
      location: `(${latitude}${','}${longitude})`,
      remark: remark,
      device: device,
      clocked_out_image: imgDataClockOut,
    }
    // console.log('outputJson----', outputJson)
    let baseIp = "https://erp-ice.com/api/v1/attendances/clock_out";
    let postData = {
      attendance: {
        location: `(${latitude}${','}${longitude})`,
        remark: remark,
        device: device,
        clocked_out_image: imgDataClockOut
      }
    };
    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json',
        'access-token': token,
        'client': client,
        'uid': uid
      }
    };
    axios.post(baseIp, postData, axiosConfig)
      .then((res) => {
        console.log(res,'--------')
        if (res.status === 200) {
          this.setState({
            clockInStatus: false,
            isSubmit: false,
            remark: '',
            device: '',
            srcImgClockOut: null
          })
          AsyncStorage.removeItem('CLOCKIN_STATUS');
          navigateAction('home')
        } else {
          this.setState({
            clockInStatus: false,
            isSubmit: false,
            remark: '',
            device: '',
            srcImgClockOut: null,
          })
        }
        console.log("RESPONSE RECEIVED: clock out", res);

      })
      .catch((err) => {
        console.log("AXIOS ERROR: ", err);
      })
  }

  // otherApi(){
  //   axios.post(baseIp, {
  //     attendance:{
  //       "location": `(${latitude}${','}${longitude})`,
  //       "remark": remark,
  //       "device": device,
  //     },
  //     headers: {
  //       'Content-Type': "application/json",
  //       'access-token': token, 
  //       'client': client,
  //       'uid': uid
  //     },

  //   }).then((res) => {
  //     console.log("clockIn-----res", res);
  //   }).catch(function (err) { 
  //     console.log('outputJson----',outputJson)
  //     alert(err) 
  //   })
  // }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'remark') { this.setState({ remark: text }) }
    if (field === 'device') { this.setState({ device: text }) }
  }

  /*********************************************** _renderHistory *********************************************/
  _renderHistory = ({ item }) => {
    return (
      <List>
        <ListItem avatar>
          <Left>
            <View style={[styles.statusBtn, { backgroundColor: item.type === 'in' ? 'green' : 'red', }]} />
          </Left>
          <Body>
            <Text>{item.status} - {item.time}</Text>
            <Text note numberOfLines={1} style={{ color: 'gray', marginTop: 5 }}>{item.location}</Text>
          </Body>
        </ListItem>
      </List>
    )
  }

  render() {
    const { time, date, remark, device, latitude, longitude, srcImgClockIn, srcImgClockOut, clockInStatus, isSubmit } = this.state;
    const statusClock = this.props.navigation.state.params.clockInStatus;
    // console.log('date', time)
    //console.log('latitude---', latitude, longitude)
    console.log('statusClock in-----', statusClock)


    return (
      <View style={styles.container}>
        {/************************** submit loading ****************************/}
        {
          isSubmit ?
            <Modal
              transparent={true}
              animationType={'none'}
              visible={isSubmit}
              onRequestClose={() => { console.log('close modal') }}>
              <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                  <ActivityIndicator color="primary" size="large" animating={isSubmit} />
                </View>
              </View>
            </Modal>
            : null
        }
        <ScrollView>

          {/******************************** TOP HEADER ******************************* */}
          <View style={styles.backgroundSty}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: RFPercentage('5'), fontWeight: '500', color: '#fff' }}>{time}</Text>
            </View>
            <Text style={{ fontWeight: '500', marginBottom: 5, marginTop: 10, color: '#fff' }}>{date}</Text>
          </View>

          {/******************************** SUBMIT FORM ******************************* */}
          <View style={{ padding: 20, }}>
            {/* <View style={styles.contentContainer}>
              <Text note style={{ color: 'blue', textAlign: 'center', fontSize: RFPercentage('2.5') }}>
                Hello Lucas
              </Text>
            </View> */}

            {/********************* REMARK *******************/}
            <Card style={{ padding: 10, marginBottom: 5, borderRadius: 10 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>1. Remark</Text>
                {/* <Text style={{ color: 'red' }}>*</Text> */}
              </View>
              <TextInput
                value={remark}
                onChangeText={(text) => this.onChangeTextInput(text, 'remark')}
                placeholder="Please fill in your reason"
                style={styles.textArea}
                underlineColorAndroid="transparent"
                multiline={true}
              />
            </Card>
            {/********************* DEVICE *******************/}
            {/* <Card style={{ padding: 10, marginBottom: 5 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 5 }}>2. Device</Text>
                <Text style={{ color: 'red' }}>*</Text>
              </View>
              <TextInput
                value={device}
                placeholder=""
                placeholderTextColor="#8c8c8c"
                style={styles.inputsty}
                onChangeText={(text) => this.onChangeTextInput(text, 'device')}
                autoCorrect={false}
                autoCapitalize="none"
              // keyboardType={'numeric'}
              />
            </Card> */}
            {/********************* CLOCK STATUS IMAGE *******************/}
            {
              statusClock === '1' || clockInStatus === true ?
                <Card style={{ padding: 10, marginBottom: 5, borderRadius: 10 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ marginRight: 5 }}>3. Clock Out Image</Text>
                    {/* <Text style={{ color: 'red' }}>*</Text> */}
                  </View>
                  {
                    srcImgClockOut === null ? null :
                      <View style={{ width: 100, height: 100, marginTop: 10, }}>
                        <Thumbnail source={srcImgClockOut} style={styles.thumbnailImg} />
                      </View>
                  }
                  <Button full style={{ marginTop: 10 }} onPress={() => this.takePhotoClockOut()}>
                    <Text>Add File</Text>
                  </Button>
                </Card>
                :
                <Card style={{ padding: 10, marginBottom: 5, borderRadius: 10 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ marginRight: 5 }}>3. Clock In Image</Text>
                    {/* <Text style={{ color: 'red' }}>*</Text> */}
                  </View>
                  {
                    srcImgClockIn === null ? null :
                      <View style={{ width: 100, height: 100, marginTop: 10, }}>
                        <Thumbnail source={srcImgClockIn} style={styles.thumbnailImg} />
                      </View>
                  }
                  <Button full style={{ marginTop: 10 ,borderRadius: 8}} onPress={() => this.takePhotoClockIn()}>
                    <Text>SELFIE</Text>
                  </Button>
                </Card>

            }

            <View style={{ alignItems: 'center', marginTop: 30 }}>
              <TouchableOpacity onPress={() => clockInStatus === true || statusClock === '1' ? this.clockOut() : this.clockIn()}>
                <View style={[styles.clockShape, {
                  borderColor: clockInStatus === true || statusClock === '1' ? 'red' : '#44c1ac',
                }]}>
                  <View style={[styles.clockInBtn, {
                    backgroundColor: clockInStatus === true || statusClock === '1' ? 'red' : '#44c1ac'
                  }]}
                  >
                    {
                      clockInStatus === true || statusClock === '1' ?
                        <Text style={{ color: '#fff', fontSize: RFPercentage('2.5'), }}>Clock Out</Text>
                        :
                        <Text style={{ color: '#fff', fontSize: RFPercentage('2.5'), }}>Clock In</Text>
                    }
                  </View>
                </View>
              </TouchableOpacity>
            </View>


          </View>

          {/******************************** CLOCK IN/OUT HISTORY ******************************* */}
          {/* <FlatList
            data={data}
            extraData={this.state}
            renderItem={this._renderHistory}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ marginTop: 10, paddingBottom: 50 }}
          /> */}

        </ScrollView>
        {/********************* BUTTON SUBMIT *******************/}
        {/* <Button full style={{ backgroundColor: clockInStatus === true || statusClock === '1' ? 'red' : '#44c1ac', height: 60 }}
          onPress={() => clockInStatus === true || statusClock === '1' ? this.clockOut() : this.clockIn()}
        >
          {
            clockInStatus === true || statusClock === '1' ? <Text>Clock Out</Text> : <Text>Clock In</Text>

          }
        </Button> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
  backgroundSty: { backgroundColor: '#44c1ac', height: 150, padding: 20, alignItems: 'center', justifyContent: 'center' },
  contentContainer: { marginBottom: 10 },
  clockShape: {
    borderRadius: 100, width: 120, height: 120, padding: 5, borderWidth: 1,

  },
  clockInBtn: {
    width: '100%', height: '100%', borderRadius: 100,
    alignItems: 'center', justifyContent: 'center'
  },
  statusBtn: { width: 8, height: 8, borderRadius: 50 },
  inputsty: { padding: 5, paddingLeft: 0 },
  textArea: {
    height: 50,
    justifyContent: "flex-start", marginTop: 5, paddingLeft: 0
  },
  thumbnailImg: { width: '100%', height: '100%', borderRadius: 0 },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },

})

export default AttendanceScreen;
