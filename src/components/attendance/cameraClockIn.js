import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, ImageBackground, Platform, Image, FlatList, ScrollView, TextInput } from 'react-native';
import { Card, Text, Thumbnail, Textarea } from 'native-base';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ImagePicker from 'react-native-image-picker';
import FA from 'react-native-vector-icons/FontAwesome';


class CameraClockInScreen extends Component { 
  constructor(props) {
    super(props);
    this.state = {
      imagePath: '',
      imageData: '',
      imageUri: ''
    };
  }

  /*********************************************** takePhoto *********************************************/
  takePhoto() {
    // console.log('launchCamera-----')
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };
        console.log('response', JSON.stringify(response));
        this.setState({
          imagePath: response,
          imageData: response.data,
          imageUri: response.uri
        });
      }
    });

  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          {/******************************** TAKE IMAGE ******************************* */}
          <View style={styles.photoContainer}>
            <TouchableOpacity style={styles.cameraPress} onPress={() => this.takePhoto()}>
              <FA name="camera" size={30} style={styles.iconSty} />
            </TouchableOpacity>
          </View>

          {/******************************** SUBMIT FORM ******************************* */}
          <View style={{ padding: 20 }}>
            <Card style={styles.formSty}>
              <Text>Reason <Text style={{ color: 'red', }}>*</Text></Text>
              <Textarea rowSpan={5} placeholder="Please fill in your reason" style={{ marginTop: 5, paddingLeft: 0 }} />
            </Card>
          </View>
        </ScrollView>

        {/******************************** SUBMIT BUTTON ******************************* */}
        <TouchableOpacity style={styles.submitBtn}>
          <Text style={{ color: '#fff' }}>CLOCK IN</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
  iconSty: { color: '#44c1ac' },
  cameraPress: {
    width: 80, height: 80, alignItems: 'center', justifyContent: 'center',
    backgroundColor: '#fff', borderRadius: 50,
  },
  photoContainer: { backgroundColor: '#eee', height: 250, alignItems: 'center', justifyContent: 'center', },
  submitBtn: {
    height: 60, backgroundColor: '#44c1ac', alignItems: 'center',
    justifyContent: 'center', position: 'absolute', width: '100%', bottom: 0
  },
  formSty: { paddingRight: 20, paddingLeft: 20, paddingTop: 20, paddingBottom: 10, marginBottom: 10, marginTop: 10 },

})

export default CameraClockInScreen;
